# SaveLoad
## Version 1.2
## © Xecestel
## MIT License

## Overview
Made with Godot Engine 3.2.
This module is just a script that allows the user to save and load data. It also allows the user to store the data on encrypted files, allowing them to make their own encryption key.  
  
## Configuration
The first thing to do is, of course, to set this as an AutoLoad from your ProjectSettings. Put this however you like: as you will need to call it from outside of it, there's no actual need to put it below every other AutoLoad.  
The only thing you need to do inside the script is to set its constants (see below for more info).  
Note that this script will only save data contained in `/root/` node children. This includes every AutoLoad and the current scene root node. It only works if the node has a method called `save_data` and a method called `load_data`, so be sure to add them on your nodes and customize them however you like.  
  
## How does it work?
The SaveLoad script uses three constants and three methods:  
<table>
	<tr>
		<td>Property</td>
		<td>Description</td>
	</tr>
	<tr>
		<td><code>const SAVE_DIR_PATH : String</code></td>
		<td>A String constant containing the path of the save directory. This allows the usage of subdirectories.</td>
	</tr>
	<tr>
		<td><code>const FILE_NAME : String</code></td>
		<td>A String containing the name of the save file.</td>
	</tr>
	<tr>
		<td><code>const ENCRYPTION_KEY : String</code></td>
		<td>This is the encryption key in case the user wants to encrypt the file. If left blank or null, the script won't encrypt the file.</td>
	</tr>
	<tr>
		<td><code>func save_game() -> void`</td>
		<td>This method checks all the children of the <code>/root/</code> node and get the save data from all the nodes containing a method called <code>save_data</code>. Then it stores the resulting Dictionary into a file. It automatically creates the file if it doesn't already exist.</td>
	</tr>
	<tr>
		<td><code>func load_game() -> bool</code></td>
		<td>This method gets all the data contained in a saved file, if it exists, and stores them on the respective <code>/root/</code> children. It returns <code>true</code> if it succeeds.</td>
	</tr>
	<tr>
		<td><code>func file_exists() -> bool</code></td>
		<td>This method returns <code>true</code> if the save file exists in the given path.</td>
	</tr>
</table>  
  
## Credits
Designed and programmed by Celeste Privitera (@xecestel).  
  
## Licenses
SaveLoad  
Copyright © 2020 Celeste Privitera  
This software is an open source project licensed under MIT License. Check the LICENSE file for more info.  
  
## Changelog
  
### Version 1.0
- Initial Commit

### Version 1.1
- Added game version variable

### Version 1.2
- Added game version check
