################################################################################
# SAVELOAD SCRIPT FOR GODOT ENGINE 3
# Version 1.2
# © Xecestel 2023
# MIT LICENSE
################################################################################
#
# MIT License
#
# Copyright © 2020-2023 Celeste Privitera
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
################################################################################

extends Node

# Constants

const GAME_VERSION : float = 0.0

const SAVE_DIR_PATH : String = "user://Saves/"
const FILE_NAME : String = "savegame.save"
const ENCRYPTION_KEY : String = "Hq9NanvFl7"

##########

# Signals

signal game_save_success()
signal game_save_failed(error)

signal load_save_success()
signal load_save_failed(error)

##########


func save_game() -> void:
	var path = SAVE_DIR_PATH + FILE_NAME
	var save_game = File.new()
	var save_dir = Directory.new()
	
	if save_dir.dir_exists(SAVE_DIR_PATH) == false:
		save_dir.open("user://")
		save_dir.make_dir("Saves")
	
	var error
	if ENCRYPTION_KEY != null and ENCRYPTION_KEY != "":
		error = save_game.open_encrypted_with_pass(path, File.WRITE, ENCRYPTION_KEY)
	else:
		error = save_game.open(path, File.WRITE)
	
	if error != OK:
		emit_signal("game_save_failed", error)
		return
	
	var data : Dictionary = {}
	
	data["version"] = GAME_VERSION
	
	for node in get_parent().get_children():
		if node.has_method("save_data"):
			data[node.name] = node.save_data()
	
	save_game.store_line(to_json(data))
	save_game.close()
	emit_signal("game_save_success")


func load_game() -> bool:
	var path = SAVE_DIR_PATH + FILE_NAME
	var save_game = File.new()
	if save_game.file_exists(path) == false:
		print_debug("File not found.")
		emit_signal("load_save_failed")
		return false #Error! Nothing to load!
	
	var error
	if ENCRYPTION_KEY != null and ENCRYPTION_KEY != "":
		error = save_game.open_encrypted_with_pass(path, File.READ, ENCRYPTION_KEY)
	else:
		error = save_game.open(path, File.READ)

	if error != OK:
		emit_signal("load_save_failed", error)
		return false
	
	# Getting data from file
	var data = parse_json(save_game.get_line())
	# Closing file
	save_game.close()
	
	# Check save version
	var save_version = data.get("version", -1.0)
	if save_version < GAME_VERSION:
		print_debug("Game version mismatch")
		emit_signal("load_save_failed", -1)
		return false
	
	for autoload in data.keys():
		get_parent().get_node(autoload).load_data(data[autoload])
		
	emit_signal("load_save_success")
	return true


func file_exists() -> bool:
	var filePath = SAVE_DIR_PATH + FILE_NAME
	var file = File.new()

	return file.file_exists(filePath)
